import React from "react";

function PlanetDescription({planetToDisplay}){
    return (<div>{planetToDisplay.name}</div>)
}
/*
=function PlanetDescription(props){
    return (<div>{props.planetToDisplay.name}</div>)
}
*/

export default PlanetDescription;
