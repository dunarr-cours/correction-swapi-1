import React, {useState} from 'react';
import './App.css';
import Axios from "axios";
import PlanetDescription from "./PlanetDescription";
function App() {

    const [character, setCharacter] = useState(null)
    const [planet, setPlanet] = useState(null)
    const [characterId, setCharacterId] = useState("")
    const [films, setFilms] = useState([])

    function handleChangeCaracterId(event){
        setCharacterId(event.target.value)
    }
    function loadCharacter(){
        Axios.get(`http://swapi.dev/api/people/${characterId}`)
            .then(
                function (response) {
                    setCharacter(response.data)
                    loadHomeworld(response.data)
                    loadFilms(response.data)
                }
            )
    }
    function loadHomeworld(characterData){
        Axios.get(characterData.homeworld)
            .then(
                ({data}) => setPlanet(data) // = response => setPlanet(response.data)
            )
    }
    function loadFilms(characterData){
        characterData.films.forEach(function (filmUrl){
            Axios.get(filmUrl)
                .then(
                  function (response) {
                      films.push(response.data)
                      setFilms([...films])
                  }
                )
        })
    }
    return (
        <div className="App">
          <input type="number"
                 value={characterId}
                 onChange={handleChangeCaracterId}
          />
          <button onClick={loadCharacter}>Chercher</button>
            {character?<h1>{character.name}</h1>:""}
            {planet?<PlanetDescription planetToDisplay={planet}/>:""}
            <div>{films.map(film => <div>{film.title}</div>)}</div>
        </div>
    );
}

export default App;
